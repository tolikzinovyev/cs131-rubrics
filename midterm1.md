## Question 1
* 5 points each
* 5 points if the answer and explanation are correct

### a)
* 2.5 points if only the answer is correct
* 0 points otherwise

### b)
* 4 points if judgement seems correct but a counterexample is not provided
* 2.5 points if only the answer is correct
* 0 points otherwise

### c)
* 4 points if their proof has hit a snag but is in the right direction
* 2.5 points if only the answer is correct
* 0 points otherwise

### d)
* 2.5 points if only the answer is correct
* 0 points otherwise

## Question 2
* 5 points each
* Give 0 or 5 points.
Use your judgement.

## Question 3
* 5 points each
* 2.5 points if there are minor mistakes

## Question 4
* 5 points each
* Give 0 or 5 points.
Use your judgement.

## Question 5
There are two directions to prove.
Give 0-5 points for each direction based on your judgement.
Just be consistent.

## Question 6
Give 2 points to everybody who start the proof in the right way, which is:

Suppose $`\sqrt{7}`$ is rational. Then there exist integers $`a,b`$ such that
$`\sqrt{7} = \frac{a}{b}`$ and $`a,b`$ are coprime.

Give the remaining 8 points based on your judgement.
Just be consistent.

## Question 7
There are two directions to prove.
Give 0-5 points for each direction based on your judgement.
Just be consistent.
